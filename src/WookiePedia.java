
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;

import javax.swing.*;
public class WookiePedia extends JFrame implements ActionListener, WindowListener, MouseListener{
	
	
	
	private static final long serialVersionUID = 1L;
	private JTextArea label2, label1;
	private JComboBox TypeSort, SelectBox;
	private JPanel forComboBoxes;
	private JTabbedPane tabby;
	private JButton newChar, saberCursorB, blasterCursorB;
	private Cursor SaberCursor, BlasterCursor;
	private String FILENAME = "StarWarsInfo2.txt";
	private String[][] tableArray, statistics;
	private AePlayWave saberup, saberdown, exitSound, saberClick, blasterClick;
	String[] headers = {"Name","Homeworld","Species","Gender","Occupation"}, statHeaders = {"Gender","Count"};
	//private String[][] allInfoData ;
	private Map<String, Map<String, Set<Person>>> mainMappy;
	//private int num=0;
	public WookiePedia(){
		super("Wookie-Pedia");
		
		//JFrame.setDefaultLookAndFeelDecorated(false);
		
		saberup = new AePlayWave("sabreup.wav");
		saberdown = new AePlayWave("sabredown.wav");
		exitSound = new AePlayWave("force1 exit sound.wav");
		saberClick = new AePlayWave("light_saber strike.wav");
		blasterClick = new AePlayWave("Blaster Sound.wav");
		
		this.addWindowListener(this);
		
		Toolkit toolkit = Toolkit.getDefaultToolkit();
    	Image imageSaber = toolkit.getImage("Light Saber.gif");
    	imageSaber = imageSaber.getScaledInstance(20, 20, Image.SCALE_DEFAULT);
    	SaberCursor = toolkit.createCustomCursor(imageSaber , new Point(0,0), "img");
    	
    	
    	Image imageBlaster = toolkit.getImage("han_solo_blaster.gif");
    	imageBlaster = imageBlaster.getScaledInstance(20, 20, Image.SCALE_DEFAULT);
    	BlasterCursor = toolkit.createCustomCursor(imageBlaster , new Point(0,0), "img");
    	this.setCursor (BlasterCursor);
    	
		
    	tabby = new JTabbedPane();
    	
    	newChar = new JButton("Push here for a new Character");
    	newChar.addActionListener(this);
    	this.add(newChar, BorderLayout.SOUTH);
    	
    	saberCursorB = new JButton("Choose your LightSaber", new ImageIcon("mini Light Saber2.gif"));
    	saberCursorB.addActionListener(this);
    	blasterCursorB = new JButton("Choose your Blaster", new ImageIcon("mini blaster.gif"));
    	blasterCursorB.addActionListener(this);
    	//saberCursorB.addMouseListener(this);
    	tableArray = new String[100][5];
    	statistics = new String[100][2];
    	
		String[] TypeSortItems = {"All","Homeworld","Species","Gender","Occupation"};
		label1 = new JTextArea("Sort By:");
		label2 = new JTextArea("Category:");
		label1.setEditable(false);
		label2.setEditable(false);
		TypeSort=new JComboBox(TypeSortItems);
		TypeSort.addActionListener(this);
		SelectBox=new JComboBox();
		SelectBox.addActionListener(this);
		
		forComboBoxes = new JPanel();
		ImageIcon imgIcon = new ImageIcon("wikipedia symbol.JPG");
		Image img = imgIcon.getImage();
		this.setIconImage(img);
		
		mainMappy = new TreeMap<String, Map<String, Set<Person>>>();
		mainMappy.put("All",new TreeMap<String, Set<Person>>());
		mainMappy.get("All").put("All",new HashSet<Person>());
		mainMappy.put("Homeworld",new TreeMap<String, Set<Person>>());
		mainMappy.put("Species",new TreeMap<String, Set<Person>>());
		mainMappy.put("Gender",new TreeMap<String, Set<Person>>());
		mainMappy.put("Occupation",new TreeMap<String, Set<Person>>());
		//allInfoData = new String[100][5];
		this.readFromFile();
		
		//System.out.println(mainMappy.get("All").get("All"));
		
		forComboBoxes.setLayout(new GridLayout(3,2));
		forComboBoxes.add(blasterCursorB);
		forComboBoxes.add(saberCursorB);
		forComboBoxes.add(label1);
		forComboBoxes.add(label2);
		forComboBoxes.add(TypeSort);
		forComboBoxes.add(SelectBox);
		this.add(forComboBoxes, BorderLayout.NORTH);
		
		
		
		int count = 0;
		for (Person p : mainMappy.get("All").get("All")){
			tableArray[count][0]=p.getName();
			//System.out.println(p.getName());
			tableArray[count][1]=p.getHomeworld();
			//System.out.println(p.getSpecies());
			tableArray[count][2]=p.getSpecies();
			tableArray[count][3]=p.getGender();
			tableArray[count][4]=p.getOccupation();
			count++;
		}
		tabby.addTab("General Info",new JScrollPane(new JTable(tableArray, headers)));
		this.add(tabby, BorderLayout.CENTER);
		
		
		
		this.setVisible(true);
		//this.setSize(100,100);
		this.setExtendedState(Frame.MAXIMIZED_BOTH);
		this.setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
	}
	public void actionPerformed(ActionEvent arg0) {
		if(arg0.getSource().equals(TypeSort)){
			SelectBox.removeActionListener(this);
			SelectBox.removeAllItems();
			if(!TypeSort.getSelectedItem().equals("All")){
				
				//String[] temp = (String[])mainMappy.get(JCB.getSelectedItem()).keySet().toArray();ASK REED!!!
				statHeaders[0]= (String)TypeSort.getSelectedItem();
				for(int i=0; i<100; i++)
					for(int j=0; j<2; j++)
						statistics[i][j]=null;
				int count = 0;
				for(String s: mainMappy.get(TypeSort.getSelectedItem()).keySet()){
					statistics[count][0] = s;
					statistics[count][1] = "" + mainMappy.get(TypeSort.getSelectedItem()).get(s).size();
					count++;
				}
				try {tabby.removeTabAt(1);}catch(Exception ex){}
				tabby.addTab("statistics",new JScrollPane(new JTable(statistics, statHeaders)));
				for(int i=0; i<mainMappy.get(TypeSort.getSelectedItem()).keySet().toArray().length;i++)
					SelectBox.addItem(mainMappy.get(TypeSort.getSelectedItem()).keySet().toArray()[i]);
			}
			//if it was all they selected
			else{
				try {tabby.removeTabAt(1);}
				catch(Exception ex){
					
				}
				for(int i=0; i<100; i++)
					for(int j=0; j<5; j++)
						tableArray[i][j]=null;
				
				int count = 0;
				for (Person p : mainMappy.get("All").get("All")){
					tableArray[count][0]=p.getName();
					tableArray[count][1]=p.getHomeworld();
					tableArray[count][2]=p.getSpecies();
					tableArray[count][3]=p.getGender();
					tableArray[count][4]=p.getOccupation();
					count++;
				}
				
				this.repaint();
				
			}
			SelectBox.addActionListener(this);
			
		}
		else if(arg0.getSource().equals(SelectBox)){
			
			
			int count = 0;
			for(int i=0; i<100; i++)
				for(int j=0; j<5; j++)
					tableArray[i][j]=null;
			for (Person p : mainMappy.get(TypeSort.getSelectedItem()).get(SelectBox.getSelectedItem())){
				tableArray[count][0]=p.getName();
				tableArray[count][1]=p.getHomeworld();
				tableArray[count][2]=p.getSpecies();
				tableArray[count][3]=p.getGender();
				tableArray[count][4]=p.getOccupation();
				count++;
			}
			
			
			
			this.repaint();
				
		}
		else if(arg0.getSource().equals(newChar)){
			
			String n = JOptionPane.showInputDialog(this, "Name:");
			if (n==null){return;}
			String h = JOptionPane.showInputDialog(this, "Homeworld:");
			if (h==null){return;}
			String s = JOptionPane.showInputDialog(this, "Species:");
			if (s==null){return;}
			String g = JOptionPane.showInputDialog(this, "Gender:");
			if (g==null){return;}
			String o = JOptionPane.showInputDialog(this, "Occupation:");
			if (o==null){return;}
			
			mainMappy.get("All").get("All").add(new Person(n,h,s,g,o));
			
			if(mainMappy.get("Homeworld").containsKey(h)){
				mainMappy.get("Homeworld").get(h).add(new Person(n,h,s,g,o));
			}
			else{
				mainMappy.get("Homeworld").put(h, new HashSet<Person>());
				mainMappy.get("Homeworld").get(h).add(new Person(n,h,s,g,o));
			}
			if(mainMappy.get("Species").containsKey(s)){
				mainMappy.get("Species").get(s).add(new Person(n,h,s,g,o));
			}
			else{
				mainMappy.get("Species").put(s, new HashSet<Person>());
				mainMappy.get("Species").get(s).add(new Person(n,h,s,g,o));
			}
			if(mainMappy.get("Gender").containsKey(g)){
				mainMappy.get("Gender").get(g).add(new Person(n,h,s,g,o));
			}
			else{
				mainMappy.get("Gender").put(g, new HashSet<Person>());
				mainMappy.get("Gender").get(g).add(new Person(n,h,s,g,o));
			}
			if(mainMappy.get("Occupation").containsKey(o)){
				mainMappy.get("Occupation").get(o).add(new Person(n,h,s,g,o));
			}
			else{
				mainMappy.get("Occupation").put(o, new HashSet<Person>());
				mainMappy.get("Occupation").get(o).add(new Person(n,h,s,g,o));
			}
			
			
			
			if(TypeSort.getSelectedItem()=="All"){
				try {tabby.removeTabAt(1);}catch(Exception ex){}
				for(int i=0; i<100; i++)
					for(int j=0; j<5; j++)
						tableArray[i][j]=null;
				
				int count = 0;
				for (Person p : mainMappy.get("All").get("All")){
					tableArray[count][0]=p.getName();
					tableArray[count][1]=p.getHomeworld();
					tableArray[count][2]=p.getSpecies();
					tableArray[count][3]=p.getGender();
					tableArray[count][4]=p.getOccupation();
					count++;
				}
			}
			else{
				int count = 0;
				for(Person p : mainMappy.get(TypeSort.getSelectedItem()).get(SelectBox.getSelectedItem())){
					tableArray[count][0]=p.getName();
					tableArray[count][1]=p.getHomeworld();
					tableArray[count][2]=p.getSpecies();
					tableArray[count][3]=p.getGender();
					tableArray[count][4]=p.getOccupation();
					count++;
				}
				count = 0;
				for(String ss: mainMappy.get(TypeSort.getSelectedItem()).keySet()){
					statistics[count][0] = ss;
					statistics[count][1] = "" + mainMappy.get(TypeSort.getSelectedItem()).get(ss).size();
					count++;
				}
			}
			
			this.repaint();
			
		}
		else if(arg0.getSource().equals(saberCursorB) && !this.getCursor().equals(SaberCursor)){
			this.setCursor(SaberCursor);
			
			saberup.run();
			//saberup.stop();
		}
		else if(arg0.getSource().equals(blasterCursorB) && !this.getCursor().equals(BlasterCursor)){
			this.setCursor(BlasterCursor);
			
			saberdown.run();
			//saberdown.();
		}
		
	}
	private void readFromFile(){
		try{
			
			
			FileReader reader = new FileReader(new File(FILENAME));
			BufferedReader buff = new BufferedReader(reader);
			String line = buff.readLine(); //that's the lame intro line
			
			line = buff.readLine();
			while(line!=null){
				String lines[] = line.split(":");
				//allInfoData[num][0] = lines[0];
				//allInfoData[num][1] = lines[1];
				//allInfoData[num][2] = lines[2];
				//allInfoData[num][3] = lines[3];
				//allInfoData[num][4] = lines[4];
				//num++;
				//sorts by homeworld
				mainMappy.get("All").get("All").add(new Person(lines[0], lines[1], lines[2], lines[3], lines[4]));
				if(!mainMappy.get("Homeworld").containsKey(lines[1])){
					mainMappy.get("Homeworld").put(lines[1],new HashSet<Person>());
					mainMappy.get("Homeworld").get(lines[1]).add(new Person(lines[0], lines[1], lines[2], lines[3], lines[4]));
				}
				else{
					mainMappy.get("Homeworld").get(lines[1]).add(new Person(lines[0], lines[1], lines[2], lines[3], lines[4]));
				}
				//sorts by species
				if(!mainMappy.get("Species").containsKey(lines[2])){
					mainMappy.get("Species").put(lines[2],new HashSet<Person>());
					mainMappy.get("Species").get(lines[2]).add(new Person(lines[0], lines[1], lines[2], lines[3], lines[4]));
				}
				else{
					mainMappy.get("Species").get(lines[2]).add(new Person(lines[0], lines[1], lines[2], lines[3], lines[4]));
				}
				//sorts by gender
				if(!mainMappy.get("Gender").containsKey(lines[3])){
					mainMappy.get("Gender").put(lines[3],new HashSet<Person>());
					mainMappy.get("Gender").get(lines[3]).add(new Person(lines[0], lines[1], lines[2], lines[3], lines[4]));
				}
				else{
					mainMappy.get("Gender").get(lines[3]).add(new Person(lines[0], lines[1], lines[2], lines[3], lines[4]));
				}
				//sorts by occupation
				if(!mainMappy.get("Occupation").containsKey(lines[4])){
					mainMappy.get("Occupation").put(lines[4],new HashSet<Person>());
					mainMappy.get("Occupation").get(lines[4]).add(new Person(lines[0], lines[1], lines[2], lines[3], lines[4]));
				}
				else{
					mainMappy.get("Occupation").get(lines[4]).add(new Person(lines[0], lines[1], lines[2], lines[3], lines[4]));
				}
				//System.out.println(mainMappy);
				
				this.repaint();
				//get next record
				line = buff.readLine();
			}
		}
		catch(Exception ex){
			ex.printStackTrace();
		}
	}
	private void writeToFile(){
		try{
			FileWriter file = new FileWriter("StarWarsInfo2.txt");
			PrintWriter out = new PrintWriter(file);
			
			out.println("NAME:Homeworld:Species:Gender:Position");
			
			for(Person p: mainMappy.get("All").get("All")){
				out.println(p.getName()+":"+p.getHomeworld()+":"+p.getSpecies()+":"+p.getGender()+":"+p.getOccupation());
			}			
			out.close();
		}catch(FileNotFoundException x){
			System.out.println("Can't Find It");
			System.exit(0);
		}catch(IOException i){
			System.out.println("Can't read file");
			System.exit(0);
		}
		System.exit(0);
	}
	public void windowActivated(WindowEvent arg0) {}
	public void windowClosed(WindowEvent arg0) {}
	public void windowClosing(WindowEvent arg0) {exitSound.run();
		this.writeToFile();}
	public void windowDeactivated(WindowEvent arg0) {}
	public void windowDeiconified(WindowEvent arg0) {}
	public void windowIconified(WindowEvent arg0) {}
	public void windowOpened(WindowEvent arg0) {}
	public void mouseClicked(MouseEvent arg0) {System.out.println("hi");}
	public void mouseEntered(MouseEvent arg0) {System.out.println("hi");}
	public void mouseExited(MouseEvent arg0) {System.out.println("hi");}
	public void mousePressed(MouseEvent arg0) {
		System.out.println("hi");
		if(this.getCursor().equals(SaberCursor)){
			System.out.println("hi");
			saberClick.run();
			
		}
		else if(this.getCursor().equals(BlasterCursor)){
			System.out.println("hi");
			blasterClick.run();
			
		}
		
	}
	public void mouseReleased(MouseEvent arg0) {System.out.println("hi");}
	public static void main (String[] args){new WookiePedia();}
}