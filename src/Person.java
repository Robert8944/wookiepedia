
public class Person implements Comparable<Person>{
	public String Name, Homeworld, Species, Gender, Occupation;
	public Person(String n, String h, String s, String g, String o){
		Name = n;
		Homeworld = h;
		Species = s;
		Gender = g;
		Occupation = o;
	}
	public String getName(){
		return Name;
	}
	public String getHomeworld(){
		return Homeworld;
	}
	public String getSpecies(){
		return Species;
	}
	public String getGender(){
		return Gender;
	}
	public String getOccupation(){
		return Occupation;
	}
	public String[] toArray(){
		String[] ans = new String[5];
		ans[0]=Name;
		ans[1]=Homeworld;
		ans[2]=Species;
		ans[3]=Gender;
		ans[4]=Occupation;
		return ans;
	}
	public int compareTo(Person arg0) {
		if(this.getName().equals(arg0.getName()))
			return 0;
		else
			return 1;
	}
	public String toString(){
		return Name+":"+Homeworld+":"+Species+":"+Gender+":"+Occupation;
	}
}
